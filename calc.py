from util import *
import argparse
import sys

items = ItemSets.EXAMPLE

parser = argparse.ArgumentParser(description="calculate items collection rolls")
parser.add_argument('item_csv', nargs='?')
parser.add_argument('-s', '--item-set')
parser.add_argument('-i', '--item', action='append')
args = parser.parse_args()

if args.item_csv:
    items = get_items_from_csv(args.item_csv)
elif args.item_set:
    try:
        items = getattr(ItemSets, args.item_set.upper())
    except AttributeError:
        print("Could not find built in item set: "+args.item_set.upper())
        sys.exit(1)
elif args.item:
    items = []
    for item in args.item:
        items.append(parse_item_arg(item))

print(expected_time_for_collection(items))