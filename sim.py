from util import *
import sys
import argparse

items = ItemSets.EXAMPLE

parser = argparse.ArgumentParser(description="simulate simulate collection rolls")
parser.add_argument('item_csv', nargs='?')
parser.add_argument('-q', '--frequency', action='store_true')
parser.add_argument('-t', '--iterations', type=int, default=100_000)
parser.add_argument('-s', '--item-set')
parser.add_argument('-i', '--item', action='append')
args = parser.parse_args()

if args.item_csv:
    items = get_items_from_csv(args.item_csv)
elif args.item_set:
    try:
        items = getattr(ItemSets, args.item_set.upper())
    except AttributeError:
        print("Could not find built in item set: "+args.item_set.upper())
        sys.exit(1)
elif args.item:
    items = []
    for item in args.item:
        items.append(parse_item_arg(item))

if args.frequency:
    print(simulate_iterations(items, args.iterations))
else:
    print(simulate_time_for_collection(items, args.iterations))