from util import *
import sys
import argparse
from multiprocessing import Queue, Process, cpu_count

def task(queue, func, items, iterations):
    queue.put(func(items, iterations))

items = ItemSets.EXAMPLE

parser = argparse.ArgumentParser(description="simulate simulate collection rolls")
parser.add_argument('item_csv', nargs='?')
parser.add_argument('-q', '--frequency', action='store_true')
parser.add_argument('-t', '--iterations', type=int, default=100_000)
parser.add_argument('-s', '--item-set')
parser.add_argument('-i', '--item', action='append')
args = parser.parse_args()

if args.item_csv:
    items = get_items_from_csv(args.item_csv)
elif args.item_set:
    try:
        items = getattr(ItemSets, args.item_set.upper())
    except AttributeError:
        print("Could not find built in item set: "+args.item_set.upper())
        sys.exit(1)
elif args.item:
    items = []
    for item in args.item:
        items.append(parse_item_arg(item))

if args.frequency:
    func = simulate_iterations
else:
    func = simulate_time_for_collection

num_procs = cpu_count()
iterations_per_process = math.ceil(args.iterations / num_procs)

queue = Queue()

processes = [Process(target=task, args=(queue, func, items, iterations_per_process)) for i in range(num_procs)]

for process in processes:
    process.start()

data = [queue.get() for i in range(num_procs)]

for process in processes:
    process.join()

if args.frequency:
    freq = {}
    for f in data:
        for key in f:
            if freq.has(key):
                freq[key] += f[key]
            else:
                freq[key] = f[key]

    print(freq)

else:
    print(data)
    avg = sum(data)/num_procs
    print(avg)