import math
# from random import randint
from fastrand import pcg32randint as randint
import csv

class Rate:
    def __init__(self, num: int, den: int) -> None:
        self.num = num
        self.den = den

    def __repr__(self) -> str:
        return "Rate(%d, %d)" % (self.num, self.den)

    def __str__(self) -> str:
        return "%d/%d" % (self.num, self.den)

class Item:
    def __init__(self, name: str, rate: Rate) -> None:
        self.name = name
        self.rate = rate

    def __repr__(self) -> str:
        return "Item(%s, %s)" % (self.name, repr(self.rate))

    def __str__(self) -> str:
        return "%s: %s" % (self.name, self.rate)

def roll_loot(items):
    lcm = math.lcm(*[item.rate.den for item in items])

    draw = randint(1, lcm)

    max = 0
    for item in items:
        max = max + (lcm/item.rate.den) * item.rate.num
        if (draw <= max):
            return item
        
    return None

def is_all_true(list):
    for i in list:
        if not i:
            return False
    return True

def adjusted_powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1, (1 << x)-1):
        yield [ss for mask, ss in zip(masks, s) if i & mask]

def sorted_powerset(s):
    s = adjusted_powerset(s)
    return sorted(s, key=lambda x: len(x))

def split_powerset(s):
    s = adjusted_powerset(s)
    splits = {}
    for x in s:
        if (len(x) not in splits):
            splits[len(x)] = []
        
        splits[len(x)].append(x)

    return splits

def expected_time_for_collection(items):
    m = len(items)

    probs = [item.rate.num/item.rate.den for item in items]
    total_prob = sum(probs)

    if (total_prob > 1):
        raise Exception('Total probability too high %.4f > 1' % total_prob)
    
    if (total_prob < 1):
        remaining_prob = 1 - total_prob
        probs.append(remaining_prob)
        m += 1

    # powerset, without empty set or itself
    # split into lists with the same length
    # { 1:[['a'], ['b'], ['c']]], 2:[['a', 'b'], ['b', 'c'], ['a', 'c']]}
    p = split_powerset(probs)

    s = 1
    for q in range(1, m):
        sign = math.pow(-1, m-1-q)

        for p_j in p[q]:
            s += sign * (1 / (1 + sum([-1 * j for j in p_j])))

    return s

def simulate_time_for_collection(items, iterations):
    avg = 0
    for i in range(1, iterations):
        num_rolls = roll_until_one_of_each(items)
        avg = avg + ((num_rolls - avg)/i)

    return avg


def simulate_iterations(items, iterations):
    freq = {'none': 0}

    for item in items:
        freq[item.name] = 0

    for i in range(0, iterations):
        item = roll_loot(items)
        name = 'none' if item is None else item.name
        freq[name] += 1

    return freq

def roll_until_one_of_each(items, max_rolls = 100_000_000):
    log = {}
    for item in items:
        log[item.name] = False
    
    num_rolls = 0
    while (True):
        num_rolls += 1
        if (num_rolls > max_rolls):
            raise Exception('Max rolls exceeded')
        
        item = roll_loot(items)

        if (item is None):
            continue

        log[item.name] = True

        # Check if we are missing any
        if is_all_true([log[name] for name in log]):
            return num_rolls

def get_items_from_csv(file):
    items = []
    with open(file, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            items.append(Item(row[0], int(row[1]), int(row[2])))

    return items

def parse_item_arg(arg):
    comma = arg.split(',')
    slash = arg.split('/')

    split = None

    if (len(comma) >= 2):
        split = comma

    if (len(slash) >= 2):
        split = slash

    if (split is None):
        raise Exception('Invalid item argument: '+arg)
    
    if (len(split) == 2):
        return Item(arg, Rate(int(split[0]), int(split[1])))
    
    if (len(split) == 3):
        return Item(split[0], Rate(int(split[1]), int(split[2])))
    
    raise Exception('Too many items in arg: '+arg)


class ItemSets:
    EXAMPLE = [
        Item('red', Rate(1, 2)),
        Item('blue', Rate(1, 5)),
        Item('green', Rate(1, 7)),
        Item('yellow', Rate(2, 15)),
    ]

    CLUE1 = [
        Item('rare', Rate(1, 1625))
    ]

    CLUE2 = [
        Item('rare1', Rate(1, 1625)),
        Item('rare2', Rate(1, 1625)),
    ]

    CLUE3 = [
        Item('rare1', Rate(1, 1625)),
        Item('rare2', Rate(1, 1625)),
        Item('rare3', Rate(1, 1625)),
    ]

ItemSets.CLUE_REAL = []
for i in range(0, 123):
    ItemSets.CLUE_REAL.append(Item('1/1625-%d' % i, Rate(1, 1625)))
for i in range(0, 5):
    ItemSets.CLUE_REAL.append(Item('1/8125-%d' % i, Rate(1, 8125)))
for i in range(0, 6):
    ItemSets.CLUE_REAL.append(Item('1/9750-%d' % i, Rate(1, 9750)))